﻿using UnityEngine;
public class CameraScript : MonoBehaviour
{
    public GameObject player;
    public float cameraDragAmt;
    public float cameraHeight;
    public float CAMERA_Z_POSITION = -20f;
    // Use this for initialization
    void Start()
    {
        player = null;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (player != null)
        {
            Vector3 newPos = LerpToPosition(player.transform.position, cameraDragAmt);
            newPos.y = newPos.y + cameraHeight;
            newPos.z = CAMERA_Z_POSITION;
            gameObject.transform.position = newPos;

        }
    }

    Vector3 LerpToPosition(Vector3 position, float lerpAmt)
    {
        float x = Mathf.Lerp(transform.position.x, position.x, lerpAmt);
        float y = Mathf.Lerp(transform.position.y, position.y, lerpAmt);
        float z = Mathf.Lerp(transform.position.z, position.z, lerpAmt);
        return new Vector3(x, y, z);
    }
}
