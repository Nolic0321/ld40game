﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class PlayerArrows : MonoBehaviour
{
    [SerializeField]
    GameObject playerArrowPrefab;
    Vector3 screenPos;
    Vector2 onScreenPos;
    float max;
    Camera camera;
    Dictionary<NetworkInstanceId, Transform> playerArrowDictionary;

    void Start()
    {
        camera = Camera.main;
        playerArrowDictionary = new Dictionary<NetworkInstanceId, Transform>();
    }

    void Update()
    {
        foreach (GameObject player in GameObject.FindGameObjectsWithTag("Player"))
        {
            if (player.GetComponent<NetworkIdentity>().isLocalPlayer)
            {
                continue;   //Don't need an arrow for player
            }

            NetworkIdentity netId = player.GetComponent<NetworkIdentity>();



            screenPos = camera.WorldToViewportPoint(player.transform.position); //get viewport positions

            if (screenPos.x >= 0 && screenPos.x <= 1 && screenPos.y >= 0 && screenPos.y <= 1)
            {
                Debug.Log("already on screen, don't bother with the rest!");
                return;
            }

            onScreenPos = new Vector2(screenPos.x - 0.5f, screenPos.y - 0.5f) * 2; //2D version, new mapping
            max = Mathf.Max(Mathf.Abs(onScreenPos.x), Mathf.Abs(onScreenPos.y)); //get largest offset
            onScreenPos = (onScreenPos / (max * 2)) + new Vector2(0.5f, 0.5f); //undo mapping

            if (!playerArrowDictionary.ContainsKey(netId.netId))
            {
                Quaternion arrowRotation = Quaternion.FromToRotation(Quaternion.identity.eulerAngles, (onScreenPos - new Vector2(.5f, .5f)));
                GameObject arrow = Instantiate(playerArrowPrefab, Camera.main.ViewportToScreenPoint(onScreenPos), arrowRotation);
                playerArrowDictionary.Add(netId.netId, arrow.transform);
            }
            else
            {
                Transform currentArrowTransform;
                if (playerArrowDictionary.TryGetValue(netId.netId, out currentArrowTransform))
                    currentArrowTransform.position = Camera.main.ViewportToScreenPoint(onScreenPos);
                    
            }
        }
    }
}
