﻿using UnityEngine;
using UnityEngine.Networking;

[RequireComponent(typeof(NetworkIdentity))]
public class CharacterStats : NetworkBehaviour {

    public int maxHealth;
    [SyncVar]
    private int _currentHealth;
    public int CurrentHealth
    {
        get { return _currentHealth; }
        set { _currentHealth = Mathf.Clamp(value, 0, maxHealth); }
    }

    private bool isDead = false;
    public bool IsDead
    {
        get { return isDead; }
        protected set { isDead = value; }
    }

    public Stat dmg;
    public Stat armor;
    public Stat exp;

	// Use this for initialization
	void Awake () {
        CurrentHealth = maxHealth;
	}

    [Command]
    public void CmdTakeDamage(int dmg)
    {
        TakeDamage(dmg);
    }

    [ClientRpc]
    public void RpcTakeDamage(int dmg)
    {
        TakeDamage(dmg);
    }

    public void TakeDamage(int dmg)
    {
        if (IsDead)
            return;

        dmg -= armor.GetValue();

        dmg = Mathf.Clamp(dmg, 0, int.MaxValue);
        _currentHealth -= dmg;
        Debug.LogFormat("{0} takes {1} damage", transform.name, dmg);
        if (_currentHealth <= 0)
            Die();
    }
    
    public virtual void Die()
    {
        isDead = true;
        Debug.LogFormat("{0} has died", transform.name);
        NetworkServer.Destroy(transform.gameObject);
        GameObject.Destroy(transform.gameObject);
    }
}
