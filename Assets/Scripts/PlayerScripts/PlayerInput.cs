﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(NetworkIdentity))]
public class PlayerInput : NetworkBehaviour {

    [SerializeField]
    private float speed;

    [SerializeField]
    private Transform shotSprayPrefab;
    [SerializeField]
    private Transform shotPrefab;

    private void Update()
    {
        AttackCheck();
    }
    private void FixedUpdate()
    {
        RotationCheck();
        MovementInputCheck();
    }

    private void MovementInputCheck()
    {
        float verticalMovement = Input.GetAxisRaw("Vertical");
        float horizontalMovement = Input.GetAxisRaw("Horizontal");

        Vector2 movement = new Vector2(horizontalMovement, verticalMovement);

        GetComponent<Rigidbody2D>().AddForce(movement.normalized * speed, ForceMode2D.Impulse);
    }

    void RotationCheck()
    {
        Vector3 mouseOnScreen = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        GetComponent<Rigidbody2D>().MoveRotation(AngleBetweenTwoPointsForAiming(mouseOnScreen,transform.position));
    }

    private void AttackCheck()
    {
        if (Input.GetButtonDown("Attack")) { 
            Debug.LogFormat("{0} is attacking", transform.name);
            CmdShootRayTo(Camera.main.ScreenToWorldPoint(Input.mousePosition));
            StartCoroutine(PlayerAttack());
        }
    }

    Quaternion QuaternionToMouse()
    {
        ////Get the Screen positions of the object
        Vector2 positionOnScreen = transform.position;

        ////Get the Screen position of the mouse
        Vector3 mouseOnScreen = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        //Get the angle between the points
        float angle = AngleBetweenTwoPointsForAiming(mouseOnScreen, positionOnScreen);

        //Ta Daaa
        return Quaternion.Euler(new Vector3(0f, 0f, angle));

    }
    float AngleBetweenTwoPointsForAiming(Vector3 a, Vector3 b)
    {
        return Mathf.Atan2(a.y - b.y, a.x - b.x) * Mathf.Rad2Deg;
    }

    [Client]
    IEnumerator PlayerAttack()
    {
        CmdSendAttackToServer();
        yield return null;

    }

    [ClientRpc]
    void RpcActivateMeleeAttack()
    {
        shotSprayPrefab.gameObject.SetActive(true);
        shotPrefab.gameObject.SetActive(true);
    }

    [ClientRpc]
    void RpcDectivateMeleeAttack()
    {
        shotSprayPrefab.gameObject.SetActive(false);
        shotPrefab.gameObject.SetActive(false);
    }



    [Command]
    public void CmdSendAttackToServer()
    {
        StartCoroutine(ServerAttack());
    }

    [Server]
    IEnumerator ServerAttack()
    {
        shotSprayPrefab.gameObject.SetActive(true);
        shotPrefab.gameObject.SetActive(true);
        RpcActivateMeleeAttack();
        yield return new WaitForSeconds(.1f);
        shotSprayPrefab.gameObject.SetActive(false);
        shotPrefab.gameObject.SetActive(false);
        RpcDectivateMeleeAttack();
        yield return null;
    }

    private IEnumerator AnimateShootSpray()
    {
        shotSprayPrefab.gameObject.SetActive(true);
        yield return new WaitForSeconds(.1f);
        shotSprayPrefab.gameObject.SetActive(false);
        yield return null;
    }
    [Command]
    void CmdShootRayTo(Vector2 shootTo)
    {
        RaycastHit2D hit = Physics2D.Raycast(shotSprayPrefab.transform.position, shootTo - (Vector2)transform.position);
        Debug.DrawLine(shotSprayPrefab.transform.position, shootTo);
        if(hit.collider != null && hit.transform.GetComponent<CharacterStats>() != null)
        {
            hit.transform.GetComponent<CharacterStats>().CmdTakeDamage(GetComponent<CharacterStats>().dmg.GetValue());
            Debug.LogFormat("{0} hit {1} with {2} dmg and {1} has {3} hp", transform.name, hit.transform.name, GetComponent<CharacterStats>().dmg.baseValue, hit.transform.GetComponent<CharacterStats>().CurrentHealth);
        }
    }
}
