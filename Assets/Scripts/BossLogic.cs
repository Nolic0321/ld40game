﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Pathfinding;

public class BossLogic : MonoBehaviour
{
    [SerializeField]
    private float speed = 3f;
    private Vector3[] waypoints;
    private Transform target = null;
    private Vector3 targetPos;
    private bool isRunningPath = false;
    private bool targetFound = false;
    private bool targetReach = false;

    // Use this for initialization
    private void Start()
    {
    }

    // Update is called once per frame
    private void Update()
    {

        //Find nearest block
        if (!targetFound)
            StartCoroutine(FindNearestTarget());
        //Go to that target
        if (targetFound)
            StartCoroutine(GoToTarget());
    }

    private IEnumerator FindNearestTarget()
    {
        var potentialTarget = GameObject.FindGameObjectsWithTag("Block").OrderBy(x => (x.transform.position - transform.position).sqrMagnitude).FirstOrDefault();
        if (potentialTarget == null)
            yield return null;
        target = potentialTarget.transform;
        targetFound = true;
        targetReach = false;
        yield return null;
    }

    private IEnumerator GoToTarget()
    {
        if (target && target.position != targetPos)
        {
            targetPos = target.position;
            GetComponent<Seeker>().StartPath(transform.position, target.position, OnPathFound);
        }
        yield return null;
    }

    private void OnPathFound(Path path)
    {
        //if (waypoints != path.vectorPath.ToArray())
        //{
        //    if (isRunningPath)
        //        StopAllCoroutines();
        //    waypoints = path.vectorPath.ToArray(); ;
        //}
        StartCoroutine(FollowPath(path));
    }


    IEnumerator FollowPath(Path path)
    {
        isRunningPath = true;
        foreach (Vector3 node in path.vectorPath)
        {
            if (!target)
                yield return null;

            Vector2 start = transform.position;
            float timeStartedLerping = Time.time;
            //while (!grid.ArePositionsInSameNode((Vector2)transform.position, path[i]))
            while ((Vector2)transform.position != (Vector2)node)
            {
                float timeSinceStartOfLerp = Time.time - timeStartedLerping;
                float lerpPercentage = timeSinceStartOfLerp / speed;
                //Debug.LogFormat("{0} is at {1} and is moving to {2}", transform.name, transform.position, path.ToString());
                GetComponent<Rigidbody2D>().MovePosition(Vector2.Lerp(start, node, lerpPercentage));
                //transform.position = Vector2.Lerp(transform.position, path[i], lerpPercentage);
                yield return new WaitForFixedUpdate();
            }
            transform.position = node;
            yield return new WaitForFixedUpdate();
        }
        //Target Reached
        TargetReached();
        isRunningPath = false;

        yield return null;
    }

    private void TargetReached()
    {
        if (target)
            Debug.LogFormat("{0} has reached their target {1}", transform.name, target.name);

        targetReach = true;


        //Do Something to the Target
        targetFound = false;

        //Reset our target (probably go find a new one?)
        target = null;
    }


}
